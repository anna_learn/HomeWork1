﻿Console.WriteLine("Привет, я инетрактивный бот системы планирования семейного бюджета. Для работы доступны команды: /start /help /info /exit");
Console.WriteLine("Введите команду:");
string inputCommand = Console.ReadLine();
string userName = "";
bool bExit = false;
bool bStart = false;

while (bExit != true)
{
    if (inputCommand != null)
    {
        var parametrs = inputCommand.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        if (parametrs.Length > 0)
        {
            switch (parametrs[0])
            {
                case "/start":
                    if (bStart == true)
                    {
                        Greetings(userName ?? "", "Бот уже запущен");
                    }
                    else
                    {
                        Console.WriteLine("Представьтесь, пожалуста. Как Вас зовут?");
                        userName = Console.ReadLine();
                        bStart = true;
                    }
                    break;
                case "/help":
                    Greetings(userName ?? "", "Вам доступны следующие команды:");
                    Console.WriteLine("/start - запуск бота");
                    Console.WriteLine("/help - вывод справки");
                    Console.WriteLine("/info - вывод информации о версии бота");
                    Console.WriteLine("/exit - выход из бота");
                    Console.WriteLine("/echo - Выводит текст операнда (доступно только после запуска бота). Формат /echo <операнд>");
                    break;
                case "/info":
                    Greetings(userName ?? "", "У Вас установлено следующая версия ПО:");
                    Console.WriteLine("BabloBot v1.0 от 19.11.2023");
                    break;
                case "/exit":
                    Greetings(userName ?? "", "Всего доброго, приходи ещё))");
                    bExit = true;
                    break;
                case "/echo":
                    if (bStart == true)
                    {
                        if (parametrs.Length == 2)
                            Console.WriteLine(parametrs[1]);
                        else
                            Console.WriteLine("Ошибка синтаксиса. См. Справку");
                    }
                    else
                        Console.WriteLine("Команда недоступна");

                    break;
                default:
                    Console.WriteLine("Неизвестная команда");
                    break;
            }
        }
    }
    Greetings(userName ?? "", "Введите команду:");
    inputCommand = Console.ReadLine();
}

static void Greetings(string userName, string text)
{
    if (userName != "")
        Console.WriteLine($"{userName}, {text}");
    else
        Console.WriteLine(text);
}